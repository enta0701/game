class Bluebird{
  int x,y;
  PImage img4;

  Bluebird(int x,int y){
    this.x = x;
    this.y = y;
  }

  void move(int blue_x,int blue_y){
    x = blue_x;
    y = blue_y;    
  }

  void draw(){
    x = x - 3;
    img4 = loadImage("bluebird.gif");
    image(img4,x,y,170,170);
  }
}