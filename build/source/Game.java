import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import hypermedia.net.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Game extends PApplet {


UDP udp;
String remoteIP = "192.168.11.33"; // Remote Computer
int LOCAL_PORT =7003;
int REMOTE_PORT=7001;

PImage img;
PImage[] s_rocket;

int ms = 0, ms_old = 0;
float y = 300;
int x = 100;
int flag = 0;
int time = 0;
long t = 0;

//\u30ed\u30b1\u30c3\u30c8\u767a\u5c04\u30d5\u30e9\u30b0
int shotFlag = 0;
int rocket_x;
int rocket_num = -1;

//\u30ed\u30b1\u30c3\u30c8\u6bb5\u6570\u8868\u793a\u7528
int rocketPosition_y = 450;
int[] rocketPosition_x = {50, 70, 90};



/*-----------------------\u9060\u85e4\u306e\u6280-----------------------*/
Rocket rocket;

/*-------------------------\u6280\uff11------------------------*/



/*-----------------------\u3042\u3079\u306e\u6280-----------------------*/
/*-------------------------\u6280\uff11------------------------*/
Be_m[] be_m = new Be_m[9999];
String getWaza1[] = new String[3];
int wx, wy, n_bm = 0;



/*-----------------------setup()-----------------------*/
public void setup() {
	size(1000, 500);
	udp=new UDP(this,LOCAL_PORT);
	udp.listen(true);
	rocket = new Rocket();
	// rocket = new PImage[3];
	s_rocket = new PImage[3];


	for (int i = 0; i < 3; i++) {
		// rocket[i] = loadImage("rocket.png");
		s_rocket[i] = loadImage("s_rocket.png");
	}


	for(int i = 0; i < 9999; i++){
    	be_m[i] = new Be_m(-100,-100);
  	}

}

/*-----------------------draw()-----------------------*/
public void draw() {
	ms = millis();

	background(0);

	for(int r = 0; r < 3; r++){
		image(s_rocket[r], rocketPosition_x[r], rocketPosition_y, 10, 30);
	}

	img = loadImage("img01.gif");
	image(img, x, y, 100, 100);

	if(flag == 1){
		y = sqrt ( 2.000f * 9.8f * y + 100) * 5 - 0.500f * 9.8f * 5 * 5;
	}

	if(shotFlag == 100){
		//\u767a\u5c04\u5f3e\u6570\u304c\uff13\u500b\u4ee5\u5185\u306a\u3089
		if(rocket_num < 3){
			// image(rocket[rocket_num], rocket_x+50, y+50, 70, 30);
			// rocket_x+=15;

			//\u753b\u9762\u67a0\u3092\u51fa\u305f\u3089\u30d5\u30e9\u30b0\u3092\u623b\u3059
			rocket.draw(rocket_x, y);
			
			if(rocket_x > 1000){shotFlag = 0;}

		}else if(rocket_num >= 3){
			println("no");
		}

	}


	for(int i = 0; i < n_bm; i++){
		be_m[i].move(wx, wy);
    	be_m[i].draw();
  	}
}

public void keyPressed() {
	String cmd="";
	switch(key) {
		case 'w':
		flag = 1;

		case 's':

		case 'a':

		case 'd':
		cmd="Hallo";

		// println(cmd);
		udp.send(cmd, remoteIP, REMOTE_PORT);
	}

}

public void keyReleased(){
	switch(key) {
		case ' ':
		shotFlag = 100;
		rocket_x = x;
		//\u30b9\u30da\u30fc\u30b9\u30ad\u30fc\u304c\u62bc\u3055\u308c\u308b\u3054\u3068\u306b\u767a\u5c04\u5f3e\u6570\u304c\u6e1b\u308b
		rocket_num++;

	}
}

//\u901a\u4fe1\u7528
public void receive(byte[] data, String ip, int port){
	n_bm++;
	data = subset(data, 0, data.length);
	String message = new String(data);
	getWaza1 = message.split(",",0);
	wx = Integer.parseInt(getWaza1[1]);
	println(wx);
	wy = Integer.parseInt(getWaza1[2]);
	println(wy);
}


// class Waza1{
// 	int x, y;

// 	PImage rocket;
// 	Waza1(){
// 		rocket = loadImage("rocket.png");
// 	}

// 	void draw(){
// 		image(rocket[rocket_num], rocket_x+50, y+50, 70, 30);
// 	}
// }


/*-----------------------\u9060\u85e4-----------------------*/
class Rocket{
	int x, y;

	PImage[] rocket;

	Rocket(){
		rocket = new Image();
	}

	draw(int rocket_x, int y){
		this.x = rocket_x;
		this.y = y;
		rocket = loadImage("rocket.png");
		image(rocket, x+50, y+50, 70, 30);
		x+=15;
	}

}

/*-----------------------\u3042\u3079-----------------------*/
//\u3042\u3079\u306e\u6575
class Be_m{
  int x,y;

  Be_m(int x,int y){
    this.x = x;
    this.y = y;
  }

  public void move(int wx, int wy){
    x = wx;
    y = wy;
  }

  public void draw(){
    x = x-5;
    fill(0,255,255);
    rect(x, y, 20, 10);
  }
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Game" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
