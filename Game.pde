/*

クラスは別ファイルに定義
メインはGame.pde

*/
import hypermedia.net.*;
UDP udp;
String remoteIP = "192.168.11.58"; // Remote Computer
int LOCAL_PORT =7003;
int REMOTE_PORT=7001;

int ms = 0, ms_old = 0;
float y = 300;
int x = 100;
int flag = 0;
int time = 0;
long t = 0;

/*-----------------------遠藤の技-----------------------*/
//キャラ
Charactor charactor;
int char_x = 125, char_y = 250;

/*-------------------------技１------------------------*/
RocketDraw rocketDraw;
int rocket_x, rocket_y;
int rocket_flag;

/*-------------------------技２------------------------*/
BigAttacDraw bigAttacDraw;
int bigman_x, bigman_y;
int bigman_flag;

/*--------------------ダメージゲージ---------------------*/
public static final int HITNUM = 100;
Hit hit[] = new Hit[HITNUM];
int[] hit_x = new int[HITNUM];
int hit_count = 0;

/*-----------------------あべの技-----------------------*/
//キャラ
Pendot pendot;

/*-------------------------技１------------------------*/
// Be_m[] be_m = new Be_m[9999];
Be_m be_m;
String getData[] = new String[3];
int wx, wy, n_bm = 0;
int pendot_x, pendot_y;

/*-------------------------技２------------------------*/
Bluebird bluebird;
int bluebird_x, bluebird_y;


/*-----------------------setup()-----------------------*/
void setup() {
	size(1000, 500);
	smooth();
	udp=new UDP(this,LOCAL_PORT);
	udp.listen(true);

	/* ================　遠藤の初期設定　================ */
	//遠藤のキャラ
	charactor = new Charactor(char_x, char_y);
	//遠藤の技１
	rocketDraw = new RocketDraw();
	//遠藤の技２
	bigAttacDraw = new BigAttacDraw();

	hit_x[0] = 0;
	for (int i = 0; i < HITNUM; ++i) {
		hit[i] = new Hit();
		if(i < HITNUM - 2){
			hit_x[i+1] = hit_x[i] + 5;
		}
	}

	/* ================　あべの初期設定　================ */
 	//あべのキャラ
 	pendot = new Pendot(800,175);
 	//あべの技１
 	be_m = new Be_m(-100,-100);
 	//あべの技２
 	// bluebird = new Bluebird(-100, -100);

 }


 /*-----------------------draw()-----------------------*/
 void draw() {
 	ms = millis();
 	background(0);
 	checkValue();

	//技の判断
	/*
	if(){
		hit_count+=1;
	}else if(){
		hit_count+=8;
	}
	*/
	hit_count *= 10;
	
	for (int i = 0; i < HITNUM - hit_count; i++) {
		hit[i].move(hit_x[i], 10);
		hit[i].draw();
	}
	if(rocket_flag == 1){
		rocketDraw.draw();
	}
	if(bigman_flag == 1){
		bigAttacDraw.draw();
	}

	// for(int i = 0; i < n_bm; i++){
	// 		be_m[i].move(wx, wy);
 	//    	be_m[i].draw();
 	//  }

	be_m.draw();
	// bluebird.draw();
 	pendot.draw();
 	charactor.draw();
}

// 画面からはみ出さないようにする
void checkValue(){         
  // if(x<0) x=0;
  // if(x>=width) x = width - 1;
  if(pendot.y < -5 ) pendot.move(0, 90);
  if(pendot.y > 355 ) pendot.move(0, -90);

  if(charactor.y < -5 ) charactor.move(0, 90);
  if(charactor.y > 450 ) charactor.move(0, -90);
}

void keyPressed() {
	String cmd="";
	switch(key) {
		case 'w':
		charactor.move(0, -90);

		char_x = charactor.x;
		char_y = charactor.y;

		cmd = "chara,"+ char_x + "," + char_y;
		udp.send(cmd, remoteIP, REMOTE_PORT);
		break;
		case 's':
		charactor.move(0, 90);

		char_x = charactor.x;
		char_y = charactor.y;

		cmd = "chara,"+ char_x + "," + char_y;
		udp.send(cmd, remoteIP, REMOTE_PORT);
		break;
	}
}

void keyReleased(){
	String cmd_waza;
	switch(key) {
		case ' ':
		rocket_x = x - 50;
		rocket_y = char_y - 50;
		rocketDraw.move(rocket_x, rocket_y);
		//スペースキーが押されるごとに発射弾数が減る
		rocket_flag = 1;

		cmd_waza = "rocket,"+ rocket_x + "," + rocket_y;
		println(cmd_waza);
		udp.send(cmd_waza, remoteIP, REMOTE_PORT);
		break;
		case 'p':
		bigman_x = x - 50;
		bigman_y = char_y - 200;
		bigAttacDraw.move(bigman_x, bigman_y);
		//スペースキーが押されるごとに発射弾数が減る
		bigman_flag = 1;

		cmd_waza = "bigman,"+ bigman_x + "," + bigman_y;
		println(cmd_waza);
		udp.send(cmd_waza, remoteIP, REMOTE_PORT);
		break;
	}
}

//通信用
void receive(byte[] data, String ip, int port){
	n_bm++;
	data = subset(data, 0, data.length);
	String message = new String(data);
	getData = message.split(",",0);

	if(getData[0].equals("be_m")){
		wx = Integer.parseInt(getData[1]);
		wy = Integer.parseInt(getData[2]);
		be_m.move(wx, wy);
	}
	
	if(getData[0].equals("pendot")){
		pendot_x = Integer.parseInt(getData[1]);
		pendot_y = Integer.parseInt(getData[2]);
		pendot.move(pendot_x, pendot_y);
	}

	if(getData[0].equals("bluebird")){
		bluebird_x = Integer.parseInt(getData[1]);
		bluebird_y = Integer.parseInt(getData[2]);
		bluebird.move(bluebird_x, bluebird_y);
	}
	
}
