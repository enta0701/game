import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import hypermedia.net.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Game extends PApplet {


UDP udp;
String remoteIP = "192.168.11.58"; // Remote Computer
int LOCAL_PORT =7003;
int REMOTE_PORT=7001;

int ms = 0, ms_old = 0;
float y = 300;
int x = 100;
int flag = 0;
int time = 0;
long t = 0;

/*-----------------------\u9060\u85e4\u306e\u6280-----------------------*/
//\u30ad\u30e3\u30e9
Charactor charactor;
int char_x = 125, char_y = 250;

/*-------------------------\u6280\uff11------------------------*/
RocketDraw rocketDraw;
//\u30ed\u30b1\u30c3\u30c8\u767a\u5c04\u30d5\u30e9\u30b0
int rocket_x, rocket_y;
int rocket_flag;

/*-------------------------\u6280\uff12------------------------*/
BigAttacDraw bigAttacDraw;
//\u30ed\u30b1\u30c3\u30c8\u767a\u5c04\u30d5\u30e9\u30b0
int bigman_x, bigman_y;
int bigman_flag;

/*--------------------\u30c0\u30e1\u30fc\u30b8\u30b2\u30fc\u30b8---------------------*/
public static final int HITNUM = 100;
Hit hit[] = new Hit[HITNUM];
int[] hit_x = new int[HITNUM];
int hit_count = 0;

/*-----------------------\u3042\u3079\u306e\u6280-----------------------*/
//\u30ad\u30e3\u30e9
Pendot pendot;

/*-------------------------\u6280\uff11------------------------*/
// Be_m[] be_m = new Be_m[9999];
Be_m be_m;
String getData[] = new String[3];
int wx, wy, n_bm = 0;
int pendot_x, pendot_y;

/*-------------------------\u6280\uff12------------------------*/
Bluebird bluebird;
int bluebird_x, bluebird_y;


/*-----------------------setup()-----------------------*/
public void setup() {
	size(1000, 500);
	smooth();
	udp=new UDP(this,LOCAL_PORT);
	udp.listen(true);

	/* ================\u3000\u9060\u85e4\u306e\u521d\u671f\u8a2d\u5b9a\u3000================ */
	//\u9060\u85e4\u306e\u30ad\u30e3\u30e9
	charactor = new Charactor(char_x, char_y);
	//\u9060\u85e4\u306e\u6280\uff11
	rocketDraw = new RocketDraw();
	//\u9060\u85e4\u306e\u6280\uff12
	bigAttacDraw = new BigAttacDraw();

	hit_x[0] = 0;
	for (int i = 0; i < HITNUM; ++i) {
		hit[i] = new Hit();
		if(i < HITNUM - 2){
			hit_x[i+1] = hit_x[i] + 5;
		}
	}

	/* ================\u3000\u3042\u3079\u306e\u521d\u671f\u8a2d\u5b9a\u3000================ */
 	//\u3042\u3079\u306e\u30ad\u30e3\u30e9
 	pendot = new Pendot(800,175);
 	//\u3042\u3079\u306e\u6280\uff11
 	be_m = new Be_m(-100,-100);
 	//\u3042\u3079\u306e\u6280\uff12
 	// bluebird = new Bluebird(-100, -100);

 }


 /*-----------------------draw()-----------------------*/
 public void draw() {
 	ms = millis();
 	background(0);
 	checkValue();

	//\u6280\u306e\u5224\u65ad
	/*
	if(){
		hit_count+=1;
	}else if(){
		hit_count+=8;
	}
	*/
	hit_count *= 10;
	
	for (int i = 0; i < HITNUM - hit_count; i++) {
		hit[i].move(hit_x[i], 10);
		hit[i].draw();
	}
	if(rocket_flag == 1){
		rocketDraw.draw();
	}
	if(bigman_flag == 1){
		bigAttacDraw.draw();
	}

	// for(int i = 0; i < n_bm; i++){
	// 	be_m[i].move(wx, wy);
 //    	be_m[i].draw();
 //  	}

	be_m.draw();
	// bluebird.draw();
 	pendot.draw();
 	charactor.draw();
}

// \u753b\u9762\u304b\u3089\u306f\u307f\u51fa\u3055\u306a\u3044\u3088\u3046\u306b\u3059\u308b
public void checkValue(){         
  // if(x<0) x=0;
  // if(x>=width) x = width - 1;
  if(pendot.y < -5 ) pendot.move(0, 90);
  if(pendot.y > 355 ) pendot.move(0, -90);

  if(charactor.y < -5 ) charactor.move(0, 90);
  if(charactor.y > 450 ) charactor.move(0, -90);
}

public void keyPressed() {
	String cmd="";
	switch(key) {
		case 'w':
		charactor.move(0, -90);

		char_x = charactor.x;
		char_y = charactor.y;

		cmd = "chara,"+ char_x + "," + char_y;
		udp.send(cmd, remoteIP, REMOTE_PORT);
		break;
		case 's':
		charactor.move(0, 90);

		char_x = charactor.x;
		char_y = charactor.y;

		cmd = "chara,"+ char_x + "," + char_y;
		udp.send(cmd, remoteIP, REMOTE_PORT);
		break;
	}
}

public void keyReleased(){
	String cmd_waza;
	switch(key) {
		case ' ':
		rocket_x = x - 50;
		rocket_y = char_y - 50;
		rocketDraw.move(rocket_x, rocket_y);
		//\u30b9\u30da\u30fc\u30b9\u30ad\u30fc\u304c\u62bc\u3055\u308c\u308b\u3054\u3068\u306b\u767a\u5c04\u5f3e\u6570\u304c\u6e1b\u308b
		rocket_flag = 1;

		cmd_waza = "rocket,"+ rocket_x + "," + rocket_y;
		println(cmd_waza);
		udp.send(cmd_waza, remoteIP, REMOTE_PORT);
		break;
		case 'p':
		bigman_x = x - 50;
		bigman_y = char_y - 200;
		bigAttacDraw.move(bigman_x, bigman_y);
		//\u30b9\u30da\u30fc\u30b9\u30ad\u30fc\u304c\u62bc\u3055\u308c\u308b\u3054\u3068\u306b\u767a\u5c04\u5f3e\u6570\u304c\u6e1b\u308b
		bigman_flag = 1;

		cmd_waza = "bigman,"+ bigman_x + "," + bigman_y;
		println(cmd_waza);
		udp.send(cmd_waza, remoteIP, REMOTE_PORT);
		break;
	}
}

//\u901a\u4fe1\u7528
public void receive(byte[] data, String ip, int port){
	n_bm++;
	data = subset(data, 0, data.length);
	String message = new String(data);
	getData = message.split(",",0);

	if(getData[0].equals("be_m")){
		wx = Integer.parseInt(getData[1]);
		wy = Integer.parseInt(getData[2]);
		be_m.move(wx, wy);
	}
	
	if(getData[0].equals("pendot")){
		pendot_x = Integer.parseInt(getData[1]);
		pendot_y = Integer.parseInt(getData[2]);
		pendot.move(pendot_x, pendot_y);
	}

	if(getData[0].equals("bluebird")){
		bluebird_x = Integer.parseInt(getData[1]);
		bluebird_y = Integer.parseInt(getData[2]);
		bluebird.move(bluebird_x, bluebird_y);
	}
	
}
//\u3042\u3079\u306e\u6575
class Be_m{
  int x,y;

  Be_m(int x,int y){
    this.x = x;
    this.y = y;
  }

  public void move(int wx, int wy){
    x = wx;
    y = wy;
  }

  public void draw(){
    x = x-10;
    fill(0,255,255);
    rect(x, y, 20, 10);
  }
}
//\u6280\uff12
class BigAttacDraw{
	int x, y;
	PImage bigman;

	BigAttacDraw(){
		
	}

	public void move(int bigman_x, int y){
		this.x = bigman_x;
		this.y = y;
	}

	public void draw(){
		bigman = loadImage("img01.gif");
		image(bigman, x+50, y+50, 300, 300);
		x+=3;
	}

}
class Bluebird{
  int x,y;
  PImage img4;

  Bluebird(int x,int y){
    this.x = x;
    this.y = y;
  }

  public void move(int blue_x,int blue_y){
    x = blue_x;
    y = blue_y;    
  }

  public void draw(){
    x = x - 3;
    img4 = loadImage("bluebird.gif");
    image(img4,x,y,170,170);
  }
}
/*-----------------------\u9060\u85e4-----------------------*/
class Charactor{
	int x, y;
	PImage img;

	Charactor(int x, int y){
		this.x = x;
		this.y = y;
	}

	public void move(int char_x, int char_y){
		x = x + char_x;
		y = y + char_y;
	}

	public void draw(){
		img = loadImage("img01.gif");
		image(img, x-50, y-50, 100, 100);
	}

}
/*----------------------\u30bf\u30e1\u30fc\u30b8---------------------*/
class Hit{
  int x, y;

  public void move(int x, int y){
    this.x = x;
    this.y = y;
  }

  public void draw(){
    fill(255, 0, 0);
    rect(x, y, 5, 30);
  }

}
/*-----------------------\u3042\u3079-----------------------*/
//\u30ad\u30e3\u30e9\u30af\u30bf\u30fc
class Pendot{
  int x,y;
  PImage img;


  Pendot(int x,int y){
    this.x = x;
    this.y = y;
    this.img = img;
  }

  public void move(int xx,int yy){
    x = x + xx;
    y = y + yy;
  }

  public void invi(){
    tint(0, 127);
  }

  public void draw(){
    img = loadImage("pen_dot.gif");
    image(img, x, y, 150,150);
  }
}
//\u6280\uff11	
class RocketDraw{
	int x, y;
	PImage rocket;

	RocketDraw(){
		
	}

	public void move(int rocket_x, int y){
		this.x = rocket_x;
		this.y = y;
	}

	public void draw(){
		rocket = loadImage("rocket.png");
		image(rocket, x+50, y+50, 70, 30);
		x+=10;
	}

}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Game" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
